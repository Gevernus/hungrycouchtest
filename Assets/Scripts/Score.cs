using System;
using System.Globalization;
using ScriptableObjects;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {
    public Text text;
    public FloatVariable score;

    private void Start() {
        score.value = 0;
    }

    private void Update() {
        text.text = score.value.ToString(CultureInfo.CurrentCulture);
    }
}