using Utils;

namespace Weapons {
    public class Rifle : Weapon {
        public override void Shoot() {
            if (!(coolDownTimer > shootCoolDown)) return;
            coolDownTimer = 0;
            var parent = transform;
            var bullet = ObjectFactory.Instance.GetObject<Bullet>(bulletPrefab);
            bullet.Shoot(parent.position, parent.up * bulletSpeed, damage);
        }
    }
}