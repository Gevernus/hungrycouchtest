using UnityEngine;

namespace Weapons {
    public abstract class Weapon : MonoBehaviour {
        [SerializeField] protected float damage;
        [SerializeField] protected float bulletSpeed;
        [SerializeField] protected float shootCoolDown;
        [SerializeField] protected Bullet bulletPrefab;
        protected float coolDownTimer;

        private void Awake() {
            coolDownTimer = shootCoolDown;
        }

        private void Update() {
            coolDownTimer += Time.deltaTime;
        }

        public abstract void Shoot();
    }
}