using System;
using UnityEngine;
using Utils;

namespace Weapons {
    [RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
    public class Bullet : PoolObject, IDamager {
        public float Damage { get; private set; }
        private Rigidbody2D _rb;

        private void Awake() {
            _rb = GetComponent<Rigidbody2D>();
        }

        public void Shoot(Vector2 startPosition, Vector2 velocity, float damage) {
            var currentTransform = transform;
            currentTransform.position = startPosition;
            currentTransform.up = velocity - startPosition;
            Damage = damage;
            _rb.velocity = velocity;
        }

        private void OnCollisionEnter2D(Collision2D other) {
            ReturnToPool();
        }
    }
}