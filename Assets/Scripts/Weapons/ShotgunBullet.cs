using UnityEngine;

namespace Weapons {
    public class ShotgunBullet : Bullet {
        public float timeToLive = 0.1f;
        private float _liveTimer;

        public override void Init() {
            base.Init();
            _liveTimer = 0;
        }

        private void Update() {
            _liveTimer += Time.deltaTime;
            if (_liveTimer > timeToLive) {
                ReturnToPool();
            }
        }
    }
}