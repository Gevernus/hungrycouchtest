using UnityEngine;
using Utils;

namespace Weapons {
    public class Shotgun : Weapon {
        public float shootAngle = 90;
        public int bulletCount = 5;

        public override void Shoot() {
            if (!(coolDownTimer > shootCoolDown)) return;
            coolDownTimer = 0;
            var parent = transform;
            float anglePerBullet = shootAngle / bulletCount;
            var angleAxis = Quaternion.AngleAxis(anglePerBullet * (bulletCount / 2), Vector3.forward);
            for (var i = 0; i < bulletCount; i++) {
                var bullet = ObjectFactory.Instance.GetObject(bulletPrefab);
                bullet.Shoot(parent.position,
                    angleAxis * parent.up * bulletSpeed, damage);
                angleAxis *= Quaternion.AngleAxis(anglePerBullet, Vector3.back);
            }
        }
    }
}