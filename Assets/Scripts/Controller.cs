﻿using UnityEngine;
using Weapons;

public class Controller : MonoBehaviour {
    public float speed;
    public Weapon[] weapons;
    private Rigidbody2D _rb;
    private Weapon _currentWeapon;

    private void Awake() {
        _rb = GetComponent<Rigidbody2D>();
        if (weapons != null && weapons.Length > 0) {
            ActivateWeapon(weapons[0]);
        }
    }

    private void Update() {
        HandleMovement();
        HandleWeaponSwitch();
        HandleShooting();
    }

    private void HandleShooting() {
        if (Input.GetMouseButton(0) && _currentWeapon) {
            _currentWeapon.Shoot();
        }
    }

    private void HandleWeaponSwitch() {
        if (Input.GetButtonDown("FirstWeapon")) {
            ActivateWeapon(weapons[0]);
        }
        else if (Input.GetButtonDown("SecondWeapon")) {
            ActivateWeapon(weapons[1]);
        }
    }

    private void ActivateWeapon(Weapon weapon) {
        if (_currentWeapon) {
            _currentWeapon.gameObject.SetActive(false);
        }

        weapon.gameObject.SetActive(true);
        _currentWeapon = weapon;
    }

    private void HandleMovement() {
        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");
        if (horizontal > 0 || horizontal < 0 || vertical < 0 || vertical > 0) {
            _rb.velocity = new Vector3(horizontal * speed, vertical * speed);
        }
    }
}