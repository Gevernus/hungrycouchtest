using UnityEngine;
using UnityEngine.Events;
using Weapons;

[RequireComponent(typeof(Collider2D))]
public class Enemy : MonoBehaviour {
    public float health;
    public float timeToRespawn;
    public UnityEvent onDeath;
    public UnityEvent onRespawn;
    private float _respawnTimer;
    private bool _isDead;
    private float _currentHealth;

    private void Start() {
        _currentHealth = health;
    }

    private void Update() {
        if (!_isDead) return;
        _respawnTimer += Time.deltaTime;
        if (_respawnTimer > timeToRespawn) {
            _isDead = false;
            _currentHealth = health;
            onRespawn.Invoke();
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.layer == LayerMask.NameToLayer("Bullet")) {
            Damage(other.gameObject.GetComponent<Bullet>().Damage);
        }
    }

    private void Damage(float damage) {
        if (_isDead) return;
        _currentHealth -= damage;
        if (_currentHealth < 0) {
            _respawnTimer = 0;
            onDeath.Invoke();
            _isDead = true;
            _currentHealth = 0;
        }
    }
}