﻿using UnityEngine;

namespace Utils {
    public class PoolObject : MonoBehaviour {
        [SerializeField] private int instanceCount;

        public int GetInitialCount() {
            return instanceCount;
        }

        public virtual void ReturnToPool(Transform parent = null) {
            ObjectFactory.Instance.ReturnObject(this, parent);
        }

        public virtual void Init() {
        }
    }
}