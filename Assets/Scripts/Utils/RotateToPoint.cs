using UnityEngine;

public class RotateToPoint : MonoBehaviour {
    public Transform point;

    private void Update() {
        transform.up = point.position - transform.position;
    }
}