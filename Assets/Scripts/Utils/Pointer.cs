using UnityEngine;

public class Pointer : MonoBehaviour {
    public Camera mainCamera;

    private void Awake() {
        Cursor.visible = false;
    }

    private void Update() {
        transform.position = (Vector2) mainCamera.ScreenToWorldPoint(Input.mousePosition);
    }
}