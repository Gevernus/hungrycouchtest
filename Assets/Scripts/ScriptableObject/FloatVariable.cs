using UnityEngine;

namespace ScriptableObjects {
    [CreateAssetMenu(menuName = "SO/Float")]
    public class FloatVariable : ScriptableObject {
        public float value;

        public void AddScore(float points) {
            value += points;
        }
    }
}